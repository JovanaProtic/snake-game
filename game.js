var canvas = document.getElementById('game');
var context = canvas.getContext('2d');
var grid = 20;
var count = 0;
var speed = document.getElementById('speedRange').value;
var score = 0;
var highScore = 0;
var resetLoop = null;

var snake = {
    x: 160,
    y: 160,

    // snake velocity. moves one grid length every frame in either the x or y direction
    dx: grid,
    dy: 0,

    // keep track of all grids the snake body occupies
    cells: [],

    // length of the snake. grows when eating an apple
    maxCells: 4
};

var apple = {
    x: 320,
    y: 320
};

function changeSpeed(value) {
    speed = value;
    document.getElementById('speedText').innerText = (60 / value).toFixed(1);
}

function changeScore() {
    score++;
    document.getElementById('score').innerText = score;
}

function resetScore() {
    score = 0;
    document.getElementById('score').innerText = score;
}

function setHighScore() {
    if (score > highScore) {
        highScore = score;
        document.getElementById('highScore').innerText = highScore;
    }
}

function gameOver() {
    resetLoop = loop;
    loop = null;
    document.getElementById('gameOverMessage').style.display = 'block';
}

function startGame() {
    if (loop == null) {
        loop = resetLoop;
    }
    resetLoop = null;
    requestAnimationFrame(loop);
    document.getElementById('gameOverMessage').style.display = 'none';
}

// get random whole numbers in a specific range
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

// game loop
function loop() {
    requestAnimationFrame(loop);
    // slow game loop to 15 fps instead of 60 (60/15 = 4)
    if (++count < speed) {
        return;
    }
    count = 0;
    context.clearRect(0, 0, canvas.width, canvas.height);
    // move snake by it's velocity
    snake.x += snake.dx;
    snake.y += snake.dy;
    // wrap snake position horizontally on edge of screen
    if (snake.x < 0) {
        snake.x = canvas.width - grid;
    }
    else if (snake.x >= canvas.width) {
        snake.x = 0;
    }

    // wrap snake position vertically on edge of screen
    if (snake.y < 0) {
        snake.y = canvas.height - grid;
    }
    else if (snake.y >= canvas.height) {
        snake.y = 0;
    }
    // keep track of where snake has been. front of the array is always the head
    snake.cells.unshift({ x: snake.x, y: snake.y });
    // remove cells as we move away from them
    if (snake.cells.length > snake.maxCells) {
        snake.cells.pop();
    }
    // draw apple
    context.fillStyle = 'red';
    context.fillRect(apple.x, apple.y, grid - 3, grid - 3);
    // draw snake one cell at a time
    context.fillStyle = 'white';
    snake.cells.forEach(function (cell, index) {

        // drawing 1 px smaller than the grid creates a grid effect in the snake body so you can see how long it is
        context.fillRect(cell.x, cell.y, grid - 3, grid - 3);

        // snake ate apple
        if (cell.x === apple.x && cell.y === apple.y) {
            snake.maxCells++;
            // canvas is 400x400 which is 25x25 grids 
            apple.x = getRandomInt(0, 25) * grid;
            apple.y = getRandomInt(0, 25) * grid;
            changeScore();
        }
        // check collision with all cells after this one (modified bubble sort)
        for (var i = index + 1; i < snake.cells.length; i++) {

            // snake occupies same space as a body part. reset game
            if (cell.x === snake.cells[i].x && cell.y === snake.cells[i].y) {
                snake.x = 160;
                snake.y = 160;
                snake.cells = [];
                snake.maxCells = 4;
                snake.dx = grid;
                snake.dy = 0;
                apple.x = getRandomInt(0, 25) * grid;
                apple.y = getRandomInt(0, 25) * grid;
                setHighScore();
                resetScore();
                gameOver();
            }
        }
    });
}

// listen to keyboard events to move the snake
document.addEventListener('keydown', function (e) {

    // left arrow key
    if (e.which === 37 && snake.dx === 0) {
        snake.dx = -grid;
        snake.dy = 0;
    }
    // up arrow key
    else if (e.which === 38 && snake.dy === 0) {
        snake.dy = -grid;
        snake.dx = 0;
    }
    // right arrow key
    else if (e.which === 39 && snake.dx === 0) {
        snake.dx = grid;
        snake.dy = 0;
    }
    // down arrow key
    else if (e.which === 40 && snake.dy === 0) {
        snake.dy = grid;
        snake.dx = 0;
    }
});

// Mobile version
document.addEventListener('touchstart', function (e) {
    var touch = e.changedTouches[0]
    startX = touch.pageX
    startY = touch.pageY
    startTime = new Date().getTime()
    e.preventDefault()
}, false)

document.addEventListener('touchmove', function (e) {
    e.preventDefault()
}, false)

document.addEventListener('touchend', function (e) {
    var touch = e.changedTouches[0]
    distX = touch.pageX - startX
    distY = touch.pageY - startY

    if (Math.abs(distX) > Math.abs(distY)) {
        if (distX > 0 && snake.dx === 0) {
            snake.dx = grid;
            snake.dy = 0;
        }
        else if (distX < 0 && snake.dx === 0) {
            snake.dx = -grid;
            snake.dy = 0;
        }
    } else {
        if (distY > 0 && snake.dy === 0) {
            snake.dy = grid;
            snake.dx = 0;
        }
        else if (distY < 0 && snake.dy === 0) {
            snake.dy = -grid;
            snake.dx = 0;
        }
    }
    e.preventDefault();

}, false)

// set values to html
document.getElementById('speedText').innerText = (60 / speed).toFixed(1);
document.getElementById('score').innerText = score;
document.getElementById('highScore').innerText = highScore;

// start the game
startGame();
